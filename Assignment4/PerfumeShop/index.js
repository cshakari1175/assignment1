const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const {check, validationResult} = require('express-validator');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/perfumeshop', 
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Order = mongoose.model('Order',
{
    name : String,
    email : String,
    phone : String, 
    address : String,
    city : String,
    postalCode : String,
    province : String,
    productOne : Number,
    productTwo : Number,
    productThree : Number,
    deliveryTime : String,
    subTotal : Number,
    tax : Number,
    total : Number
});
var myApp = express();
myApp.use(bodyParser.urlencoded({extended:false}));

myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');

myApp.get('/', function(req, res){
    res.render('index'); 
});

var phoneRegex = /^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$/;
var postalRegex = /^[A-Za-z][0-9][A-Za-z]\s?[0-9][A-Za-z][0-9]$/;

function checkRegex(userInput, regex){
    if(regex.test(userInput)){
        return true;
    }
    else{
        return false;
    }
}
function customPostal(value)
{
    if(!checkRegex(value, postalRegex))
    {
        throw new Error('Postal code in incorrect format');
    }
    return true;
}
function customPhoneValidation(value)
{
    if(!checkRegex(value, phoneRegex)){
        throw new Error('Phone should be in the format xxx-xxx-xxxx');
    }
    return true;
}

myApp.post('/', [
    check('name', 'Must include a name').notEmpty(),
    check('email', 'Must include email').isEmail(),
    check('phone').custom(customPhoneValidation),
    check('address', 'Must include address').notEmpty(),
    check('city', 'Must include city').notEmpty(),
    check('postalCode', 'Must include valid postal code').notEmpty().custom(customPostal),
    check('province', 'Must include province').notEmpty(),
    check('productOne', 'Must include valid quantity for first product').notEmpty().isInt(),
    check('productTwo', 'Must include valid quantity for second product').notEmpty().isInt(),
    check('productThree', 'Must include valid quantity for third product').notEmpty().isInt(),
    check('deliveryTime', 'Must include delivery time').notEmpty()

],function(req, res){

    const errors = validationResult(req);
    if (!errors.isEmpty()){
        res.render('index', {
            errors:errors.array()
        });
    }
    else{
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var address = req.body.address;
        var city = req.body.city;
        var postalCode = req.body.postalCode;
        var province = req.body.province;
        var productOne = req.body.productOne;
        var productTwo = req.body.productTwo;
        var productThree = req.body.productThree;
        var deliveryTime = req.body.deliveryTime;
        var tax = 0;
        var shippingCost = 0;
        switch (province)
        {
            case "alberta":
            case "nwt":
            case "nunavut":
            case "yukon":
                tax = 1.05;
                break;
            case "britishColumbia":
            case "manitoba":
                tax = 1.12;
                break;
            case "newBrunswick":
            case "newfoundland":
            case "novaScotia":
            case "pei":
                tax = 1.15;
                break;
            case "ontario":
                tax = 1.13;
                break;
            case "quebec":
                tax = 1.14975;
                break;
            case "saskatchewan":
                tax = 1.11;
                break;
        }
        switch(deliveryTime)
        {
            case "oneDay":
                shippingCost = 30;
                break;
            case "twoDays":
                shippingCost = 25;
                break;
            case "threeDays":
                shippingCost = 20;
                break;
            case "fourDays":
                shippingCost = 15;
                break;
        } 
        var productTotal = ((parseInt(productOne) * 40) + (parseInt(productTwo) * 50) + (parseInt(productThree) * 60));
        var subTotal = productTotal + shippingCost;
        tax = tax - 1;
        var total = subTotal + tax;

        var pageData = {
            name : name,
            email : email,
            phone : phone,
            phone : phone, 
            address : address, 
            city : city,                                      
            postalCode : postalCode,
            province : province,
            productOne : productOne,
            productTwo : productTwo,
            productThree : productThree, 
            deliveryTime : deliveryTime, 
            subTotal : subTotal,
            tax : tax,
            total : total
        }

        var newOrder = new Order(pageData);
        newOrder.save().then(function()
        {
            console.log('New order created');
        });
        res.render('index', pageData);
    }   
});

myApp.get('/allorders', function(req,res)
{
    Order.find({}).exec(function(err,orders)
    {
        res.render('allorders',{orders: orders});
    });
});

myApp.listen(8080);
console.log('execution complete');