1. Open "Assignment 4" folder in VS Code
2. Open the terminal and run "NPM Install" command. (CTRL + Tidle to open command window)
3. CD to the PerfumeShop Directory by typing "cd perfumeshop" and clicking enter
4. Type in "node index.js" and click enter
5. Open "localhost:8080" on your browser

## Contributers
- Chinar Shakari <cshakari1175@conestogac.on.ca>

## License

Licensed under MIT LICENSE